# docs-arquitetura
---
### Repositório destinado para estruturamento da documentação da arquitetura de software e padrões a serem utilizados. 


* #### [Documentação sobre arquitetura do JavaScript dos formulários](/programacao/arquiteturaJavascript.md)
* #### [Documentação sobre o arquivo FIEA.js](/programacao/fiea.md)
* #### [Guidelines sobre Git](/programacao/guidelineGit.md)
* #### [Desenvolvimento de módulos externos e guidelines sobre .NET](/programacao/guidelineDotNet.md)