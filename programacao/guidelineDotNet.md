# Desenvolvimento de módulos externos
---
### Para o desenvolvimento dos módulos que serão embutidos no orquestra, foi definido para o uso o framework __.NET__ em conjunto com __C#__.

## Requisitos
---
* [Microsoft .NET Framework](https://www.microsoft.com/net/download/dotnet-framework-runtime)
* [Git client](https://git-scm.com/downloads)
* Algum [editor de texto](https://code.visualstudio.com/docs/setup/windows) ou [IDE específica](https://visualstudio.microsoft.com/pt-br/downloads/)

## Configuração
---
> Considerando a instalação do framework .NET, a sua _CLI_ dispõe opções para gerar _boilerplate_ de projetos, já formatados em algumas arquiteturas/ferramentas. Estaremos utilizando a arquitetura __MVC__ através do comando `dotnet new mvc`.

> Dependendo do projeto e suas necessidades, é importante a coerência na padronização e utilização da(s) ferramenta(s), ex: Fazer uso do [.gitignore](https://git-scm.com/docs/gitignore) específico para o projeto, utilizar algum gerenciador de pacotes como o [NuGet](https://www.nuget.org/), etc.

## Arquitetura
---
![alt text](https://csharpcorner-mindcrackerinc.netdna-ssl.com/UploadFile/3d39b4/folder-structure-of-Asp-Net-mvc-project/Images/Folder-Structure-of-ASP.NET-MVC-Project1.jpg "Estrutura de pastas")

> Por seguir a arquitetura __MVC (Model-View-Controller)__, o projeto-base se divide em camadas, tais quais:

* __Model:__ Camada que representa os dados do aplicativo e suas regras de negócio.
* __View:__ Camada de exibição para o usuário, contém os componentes de interface.
* __Controller:__ Camada receptora das solicitações do navegador, encaminha as chamadas ao _Model_ e as retorna para a _View_.

#### Links úteis
----
* [Criar um aplicativo Web com o ASP.NET Core MVC no Windows com o Visual Studio](https://docs.microsoft.com/pt-br/aspnet/core/tutorials/first-mvc-app/?view=aspnetcore-2.1)
* [Documentação gitignore (en-US)](https://git-scm.com/docs/gitignore)
* [Documentação Orquestra sobre módulos](https://docs.smlbrasil.com.br/bpms/manual/gerenciaracesso/v3-help-modulos.aspx)