# FIEA.js
---
### Arquivo que contem funções genéricas que poderão ser utilizadas em todos os processos

* De forma similar a [arquitetura do JavaScript](../arquiteturaJavascript.md), haverá um objeto principal contendo suas funcionalidades
* O objeto "FIEA" contem agrupamentos, com intuito de melhor dividir responsabilidades e mapear em quais locais ficam cada bloco (eventos, métodos e funcionalidades, etc.)
* É de **extrema importância** que cada elemento do código fique em seu respectivo agrupamento, a fim de manter o padrão

### Exemplo do corpo do objeto "FIEA"
---
```javascript
var FIEA = {

    init: function () {
        // Inicialização dos métodos do corpo
        this.setEvents()
    },

    setEvents: function () {
        // Adiciona aos campos os seus respectivos eventos e comportamentos
    },

    hideAndShow: function (hide, show) {
        // Esconde ou mostra campos
        // try {
        //     //esconde
        //     //$.each(hide, function (k, v) {...})
            
        //     //mostra
        //     //$.each(show, function (k, v) {...})
        // } catch (error) {...}
    },
    hideElements: function(objArr) {
        // Função genérica para esconder elemento(s)
        // $.each(objArr,function(i,e) {...})
    },
    
    showElements: function(objArr){
        // Função genérica para mostrar elemento(s)
        // $.each(objArr,function(i,e) {...})
    }
}

$(window).load(function () {
    // Criação da instância do objeto
    FIEA.init()
})
```