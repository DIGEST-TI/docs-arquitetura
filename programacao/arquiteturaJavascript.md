# Definições da arquitetura do JavaScript
---
### Atualmente, para facilitar o estruturamento do código e sua manutenção, foi definido o seguinte padrão de arquitetura de código:

* Haverá um objeto padrão para todas as atividades, denominado **"Tall"**
* O objeto "Tall" contem atributos agrupados, com intuito de melhor dividir responsabilidades e mapear em quais locais ficam cada bloco (variáveis, eventos, métodos, etc.)
* É de **extrema importância** que cada elemento do código fique em seu respectivo agrupamento, a fim de manter o padrão
* As outras atividades do processo terão seu próprio objeto "T", relativo a sua própria numeração, ex: T01, T02, etc..


### Exemplo do corpo do objeto "Tall"
---
```javascript
var Tall = {

    // Campos do formulário a serem declarados
    btnInformacoes: null,
    inpUnidade    : null

    // Criação de agrupamentos para elementos similares/idênticos
    // Atualmente só funciona se carregar manualmente, sem usar o arquivo FIEA
    agpGinastica: {
        inpServicoPlanoDeAula        : null,
        inpResponsavelPeloAtendimento: null,
        inpTema                      : null,
        inpImpressaoDeRelatorio      : null,
        inpObjetivosDeAprendizagem   : null,
    },
    
    init: function () {
        // Método que inicializa as estruturas do objeto
        FIEA.loadFields(this)
        this.setEvents()

        // Não é mais usado com muita frequência por causa da função FIEA.loadFields
        this.loadFields()
    },

    loadFields: function () {
        // Carrega os campos em agrupamentos
        this.agpGinastica.inpServicoPlanoDeAula         = $('inp:servicoPlanoDeAula')
        this.agpGinastica.inpResponsavelPeloAtendimento = $('inp:responsavelPeloAtendimento')
        this.agpGinastica.inpTema                       = $('inp:tema')
        this.agpGinastica.inpImpressaoDeRelatorio       = $('inp:impressaoDeRelatorio')
        this.agpGinastica.inpObjetivosDeAprendizagem    = $('inp:objetivosDeAprendizagem')
    },

    setEvents: function () {
        // Adiciona aos campos os seus respectivos eventos e comportamentos
        this.btnInformacoes.on('click', () => {
            console.log('btnInformacoes action!')
        }) 

        this.inpUnidade.change(event=>{
            const unidade = event.target.value;

            if (unidade == '2787'){
                FIEA.hideNsHow(['#elementoEsconder'],['#elementoExibir']);
            }
        })
    }
}

$(window).load(function () {
    // Inicialização do objeto Tall
    Tall.init();
})
```

### Exemplo do corpo do objeto "T##"
---
```javascript
var T01 = {
    // Estrutura idêntica ao objeto "Tall"
    // Campos do formulário a serem declarados
    btnDadosEtapaX: null,
    
    init: function () {
        // Método que inicializa as estruturas do objeto
        FIEA.loadFields(this)
        this.setEvents()
    },

    setEvents: function () {
        // Adiciona aos campos os seus respectivos eventos e comportamentos
        $(this.btnDadosEtapaX).on('click', () => {
            // Pelo Tall ser uma variável global, poderá ser acessada por qualquer outra atividade, ex:
            $(Tall.btnInformacoes).trigger('click')
        }) 
    }
}

$(window).load(function () {
    // Inicialização do objeto T01
    T01.init();
})
```
## Preocauções e práticas a serem evitadas
---
### Devemos evitar algumas práticas devido a pressão, costumes e hábitos para não comprometer a integridade, disponibilidade e desempenho da aplicação.

* Uso excessivo de ``var`` para declaração de [variáveis](http://www.matera.com/blog/post/javascript-6-diferenca-entre-var-let-e-const);
* Acessar excessivamente elementos soltos do javascript, gerando código [repetitivo](https://stackoverflow.com/questions/35343068/how-to-reduce-repeating-code-in-jquery) e repetido;
* Falta do uso de [versionamento de código](https://git-scm.com/book/pt-br/v1/Primeiros-passos-Noções-Básicas-de-Git);
* Declaração de objetos e funções fora do espaço designado para o(s) mesmo(s), _ex.:_ adicionar alguma função de comportamento fora do corpo ``setEvents``;
* Declaração de objetos repetidos dos quais poderiam estar em algum grupo já existente, _ex.:_ criar um agrupamento ``tableEvents`` quando já existe um outro chamado ``setEvents``;
* Utilização excessiva de condições e de blocos fixos, o que torna o código extremamente acoplado

## Bons costumes a serem praticados e desenvolvidos
---
### E por outro lado, devemos reforçar boas práticas para garantir a melhor qualidade possível de nosso código, como exemplo:

* Analisar, refatorar e estruturar blocos repetitivos e buscar novas soluções, explorando as possibilidades e [princípios](http://www.eduardopires.net.br/2013/04/orientacao-a-objeto-solid/) da linguagem;
* Manter o código padronizado, bem identado e limpo de qualquer bloco inutilizável;
* Ser descritivo e objetivo nas declarações de código, a fim de evitar a necessidade excessiva de comentários e afins;
* Buscar novas tecnologias para serem agregadas a estrutura;
* Aproveitar as tecnologias já utilizadas para organizar e agilizar a própria produtividade (Git, IDEs, entre outros recursos)
* Uso do [modo estrito](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Strict_mode) para evitar comportamentos [inapropriados](https://www.w3schools.com/js/js_hoisting.asp) e talvez inesperados.
