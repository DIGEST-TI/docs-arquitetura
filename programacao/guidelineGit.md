# Desenvolvimento com Git
---
### A utilização do git como ferramenta de versionamento traz mais confiabilidade ao projeto e segurança para o desenvolvedor.
---
* [**Ambientes**](#ambientes)
* [**Observações**](#observações)
* [**Branches (Ramificações)**](#branches)
* [**Fluxo de desenvolvimento**](#fluxo)
---
## Ambientes
---

<a id="ambientes"/>

![alt text](https://git-scm.com/figures/18333fig0106-tn.png "Ambientes do git")

> Basicamente, o git opera entre três tipos de "ambientes" locais, dos quais fazem parte do fluxo de envio das alterações:

* _Working Directory_, ou local de trabalho
* _Stage_
* Repositório

> No _Working Directory_ entende-se que são feitas apenas alterações locais, das quais o próprio git "não reconhece a fundo" as alterações que foram feitas, apenas mapeia alterações a nível de arquivo.

> Na área de _Stage_ ficam presentes as alterações candidatas ao futuro "compromisso" que será feito, já mapeados pelo git como alterações diretas a nível de linha de código. Pode-se entender como uma área de "homologação".

> Na área de repositório já se encontram os compromissos, vulgos _commits_, que são alterações "empacotadas" e homologadas da área de Stage em commits e adicionadas a história do projeto de forma procedural. 

## Observações

<a id="observacoes"/>

---

* Em qualquer momento é possível tramitar as informações transportadas entre os ambientes, contanto, deve-se ter muito cuidado e atenção em sua manipulação
* Cada _commit_ contém um _hash_ (id) específico e único para ser identificável
* O comando `git status` sempre irá apontar as alterações e mudanças das informações nos ambientes
* A plataforma contém uma vasta [documentação](https://git-scm.com/book/pt-br/v1), oficial e não-oficial, em caso de dúvidas é sempre uma boa prática recorrer a alguma(s) fonte(s) de informação
* É importante manter uma linha de descrições padronizadas a fim de manter a organização do repositório e de sua história 

## _Branches_ (Ramificações)

<a id="branches"/>

---
![alt text](https://camo.githubusercontent.com/56cc338a438d5ea126372c7d3d17d41e15cb2856/68747470733a2f2f6261636b6c6f67746f6f6c2e636f6d2f6769742d67756964652f656e2f696d672f706f73742f7374657075702f636170747572655f737465707570315f355f362e706e67 "Ambientes do git")
### Por ter uma estrutura similar a uma árvore, o git trabalha com ramificações contendo versões baseadas em outro(s) ramo(s) (_branch_). 

> Por padrão, a branch _master_ é o ramo principal. Existem vários padrões utilizados, mas geralmente se utilizam de uma branch auxiliar chamada "develop" que serve como container para todas as alterações em desenvolvimento, que é convergido posteriormente. 

> Embora existam outras formas, o ideal é sempre criar uma nova branch atualizada antes de iniciar qualquer trabalho, para "conter" qualquer alteração feita no projeto e evitar qualquer tipo de confusão

* [Documentação de apoio sobre branches](http://blog.caelum.com.br/desmitificando-branches-remotas-com-git/) 


## Fluxo de desenvolvimento

<a id="fluxo">

---

> O fluxo ideal de desenvolvimento é por etapas, nas quais o desenvolvedor realiza seus commits de acordo com o progresso de sua tarefa, a fim de manter os repositórios local e remoto atualizados com o histórico de sua produção.

> Pode-se caracterizar como fluxo simples e padrão o seguinte:

* [Criar uma branch local](https://git-scm.com/book/pt-br/v1/Ramifica%C3%A7%C3%A3o-Branching-no-Git-B%C3%A1sico-de-Branch-e-Merge);
* Instalar dependências e configurações necessárias para iniciar o desenvolvimento;
* Após realizar a sua atividade parcialmente ou completamente, adicionar as alterações para stage com o comando `git add`;
* Ao verificar o que foi adicionado para stage, criar o "compromisso" com o commando `git commit` ; 
* Enviar o(s) novo(s) commits para o repositório remoto com o comando `git push`;
* Criar um _pull request_ para convergir as diferenças feitas em sua branch com a branch-alvo.